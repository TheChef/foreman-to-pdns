# Copyright (c) 2020 Jochem  <spam@bruijns.org>
#
# This file is part of foremantopdns .
#
# All rights reserved.

from foremantopdns.cli import main
from io import StringIO
from mock import patch, Mock


@patch('sys.stdout', new_callable=StringIO)
def test_main(mock_stdout):
    with patch('foremantopdns.config.load_settings', Mock()):
        main()
        assert "shiny new tool" in mock_stdout.getvalue()
