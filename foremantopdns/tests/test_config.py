# Copyright (c) 2020 Jochem  <spam@bruijns.org>
#
# This file is part of foremantopdns .
#
# All rights reserved.

from foremantopdns.config import load_settings
from io import StringIO
from mock import patch

from omniconf import config
from omniconf.config import DEFAULT_REGISTRY
import pytest
import os


def test_load_settings():
    DEFAULT_REGISTRY.clear()
    os.environ["EXAMPLE_SETTING"] = "foo"
    load_settings(load=True)
    assert config("example.setting") == "foo"


def test_no_load_settings():
    DEFAULT_REGISTRY.clear()
    os.environ["EXAMPLE_SETTING"] = "foo"
    load_settings(load=False)
    assert config("example.setting") == "bar"


@patch('sys.stderr', new_callable=StringIO)
def test_load_settings_version(mock_stderr):
    with patch('omniconf.backends.argparse.ARGPARSE_SOURCE', ["-v"]):
        with pytest.raises(SystemExit):
            load_settings()
        assert "foreman-to-pdns" in mock_stderr.getvalue()


@patch('sys.stdout', new_callable=StringIO)
def test_load_settings_help(mock_stdout):
    with patch('omniconf.backends.argparse.ARGPARSE_SOURCE', ["-h"]):
        with pytest.raises(SystemExit):
            load_settings()
        assert "foreman-to-pdns" in mock_stdout.getvalue()
