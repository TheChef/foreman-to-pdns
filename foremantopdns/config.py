# Copyright (c) 2020 Jochem  <spam@bruijns.org>
#
# This file is part of foremantopdns .
#
# All rights reserved.


from omniconf import setting, help_requested, show_usage, version_requested, \
    omniconf_load
import sys


def get_version():
    from pbr.version import VersionInfo
    return VersionInfo("foremantopdns").semantic_version()


def load_settings(load=True):
    setting("example.setting", _type=str, default="bar",
            help="An example to get you started.")
    if help_requested():
        show_usage(name="foreman-to-pdns")

    if version_requested():
        print("foreman-to-pdns {}".format(
            get_version().release_string()),
            file=sys.stderr)
        sys.exit(0)

    if load:
        omniconf_load(autoconfigure_prefix="")
