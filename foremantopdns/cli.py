# Copyright (c) 2020 Jochem  <spam@bruijns.org>
#
# This file is part of foremantopdns .
#
# All rights reserved.

from foremantopdns.config import load_settings


def main():
    load_settings()
    print("Your shiny new tool is ready to go!")


if __name__ == "__main__":  # pragma: nocover
    main()
