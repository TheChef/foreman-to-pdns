# Copyright (c) 2020 Jochem  <spam@bruijns.org>
#
# This file is part of foremantopdns .
#
# All rights reserved.

from setuptools import setup

setup(
    setup_requires=['pbr>=1.9', 'setuptools>=17.1'],
    pbr=True,
)
